window.onload = function() {
Player.init();	
}

Player = {
	init: function() {
		let container = document.getElementsByClassName('WebGL')[0];
		this.scene = new THREE.Scene();
		window.scene = this.scene;

		let aspect = container.offsetWidth / container.offsetHeight;
		this.camera = new THREE.PerspectiveCamera(30.0, aspect, 10, 1000);
		this.camera.position.z = 20;
		this.scene.add(this.camera);

		// Axis
		let axisHelper = new THREE.AxisHelper(500);
		this.scene.add(axisHelper);
		
		// Light
		let light = new THREE.AmbientLight(0x0000ff);
		this.scene.add(light);

		let pointLight = new THREE.PointLight(0xff0000, 1, 100);
		pointLight.position.set(10, 10, 10);
		this.scene.add(pointLight);

		let pointLightHelper = new THREE.PointLightHelper(pointLight, 2);
		this.scene.add(pointLightHelper);

		// Render
		this.renderer = new THREE.WebGLRenderer();
		container.appendChild(this.renderer.domElement);
		
		// size pixel
		this.renderer.setPixelRatio(window.devicePixelRatio);
		this.renderer.setSize(container.offsetWidth, container.offsetHeight);

		// Trackball controls
		this.controls = new THREE.TrackballControls(this.camera, container);
		this.controls.zoomSpeed = .5;
		
		// texture
		let textureLoader = new THREE.TextureLoader();
		textureLoader.load('../img/288381350.jpg', function(texture){
		// Geometry
		let geometry = new THREE.SphereGeometry(5, 30, 30);
		let material = new THREE.MeshPhongMaterial({map: texture});
		let mesh = new THREE.Mesh(geometry, material);
		this.scene.add(mesh);


		});

		this.animate();

	},

	animate: function(){
		requestAnimationFrame(this.animate.bind(this));
		this.controls.update();
		this.renderer.render(this.scene, this.camera);
	},
}

